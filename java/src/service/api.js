const BASE = "http://localhost:8090/project";

// AUTH
const AUTH = `${BASE}/auth`;
export const LOGIN = `${AUTH}/login`;
export const REGISTER = `${AUTH}/register`;

//CATEGORIES
export const CATEGORIES = `${BASE}/categories`;