import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './index.css';
import Login  from './components/auth/Login';
import Register  from './components/auth/Register';
import Category from './components/category/Categories';
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import UpdateCategory from './components/category/partials/UpdateCategory';
import AddCategory from './components/category/partials/AddCategory';
import NotFound from './components/common/NotFound';
import Home from './components/home/Home';

class App extends Component {
  render() {
    return (
        <Router>
        <Fragment>
          <Header></Header>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/categories" component={Category} />
            <Route exact path="/updateCategory/:id" component={UpdateCategory} />
            <Route exact path="/addCategory" component={AddCategory} />
            <Route exact path="/" component={Home} />
            <Route component={NotFound} />
          </Switch>
          <Footer></Footer>
      </Fragment>
    </Router>
    );
  }
}

export default App;
