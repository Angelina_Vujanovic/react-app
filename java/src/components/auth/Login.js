import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../style/components/auth/login.css';
import {LOGIN} from "../../service/api";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {email: '', password:'', errorMessage:''};
    }

    handleInputChange = (event) => {      
        const target = event.target;
        const name = target.name;
      
        this.setState({
            [name]: target.value
        });      
    }    
    
    
    handleSubmit = (event) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password
            })
        };
        
        fetch(LOGIN, requestOptions)
        .then(response => {
            if (response.ok) {
                response.json().then(data => {
                    this.setState({errorMessage: ''})
                    localStorage.setItem("user", btoa(data.email+":"+data.password));
                    this.props.history.push("/categories");  
                });
            }else {
                response.text().then(message => this.setState({errorMessage: message}))
            }
        })
        .catch(error => console.log(error))
        event.preventDefault();

    };
    render() {
    
        return (
            <div className="login_form">
                <form onSubmit={this.handleSubmit}>
                    <input 
                        type="email" 
                        name="email"
                        placeholder="Enter email"
                        onChange={this.handleInputChange} />

                    <input 
                        type="password" 
                        name="password" 
                        placeholder="Enter password"
                        onChange={this.handleInputChange} />
                                        
                    <input type="submit" value="Submit" className="submit" />
                    <label className="error">{this.state.errorMessage}</label>
                    <label className="info">If you do not have account, please <Link to="/register">register</Link></label>
                </form>
            </div>
           
        )
    }
};

export default Login;