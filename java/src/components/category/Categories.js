import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { CATEGORIES } from '../../service/api';
import Table from '../common/Table';
import Modal from '../common/Modal';
import '../../style/components/category/category.css';

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {categories: [], show: false, category: null};
    }

    componentDidMount(){

        if(localStorage.getItem('user') === null) {
            this.props.history.push('/login');
        } else {
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic '+localStorage.getItem("user")
                }
            };
            fetch(CATEGORIES+"/public", requestOptions)
            .then(response => {
                if(response.ok) {
                    response.json().then(data =>
                        this.setState({categories: data})    
                    )
                }else {
                    response.text().then(message => alert(message))
                }
            })
            .catch(error => console.log(error))
        }    
    }

    updateCategory = (id) => {
        this.props.history.push("updateCategory/"+id);
    }

    deleteCategory = (id) => {
        const path = CATEGORIES + "/" + id;
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic '+localStorage.getItem("user")
            }
        };
        fetch(path, requestOptions)
        .then(response => {
            if (response.ok) {
                response.json().then(data => 
                    this.setState({categories: this.state.categories.filter(category => category.id!==data.id)})
                );
            }else {
                response.text().then(message => alert(message))
            }
        })
        .catch(error => console.log(error))
    }
    openDetails = (id) => {
        let category = this.state.categories.find(cat => cat.id === id);
        this.setState({category, show: true});
    }

    closeDetails = () => {
        this.setState({category: null, show: false});
    }

    render() {

        const heading=["Id", "Name", "Description", "", "", ""];
        const buttons=[
            {name: "Details", action: this.openDetails, class: "btn-update"},
            {name: "Update", action: this.updateCategory, class: "btn-update"}, 
            {name: "Delete", action: this.deleteCategory, class: "btn-update"}];
        
        return (
            <div className="category_wrapper">
                <Modal show={this.state.show} onClose={this.closeDetails}>
                    {
                        this.state.category && 
                        <div className="modal_content">
                            <h3>Category details</h3>
                            <span> Id: {this.state.category.id}</span>
                            <span> Name: {this.state.category.categoryName}</span>
                            <span> Description: {this.state.category.categoryDescription}</span>
                        </div>
                    }
                </Modal>
                {
                    this.state.categories && 
                    <Table heading={heading} data={this.state.categories} buttons={buttons}></Table>
                    
                } 
                <Link to="/addCategory">Add new category</Link>
            </div>
           
        )
    }
};

export default Category;