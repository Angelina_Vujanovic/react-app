import React, { Component } from 'react';
import '../../../style/components/auth/login.css';
import { CATEGORIES } from "../../../service/api";

class UpdateCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {category: null};
    }

    componentDidMount() {
        const currentUser = localStorage.getItem("user");
        if(currentUser) {
            const path = CATEGORIES+"/"+this.props.match.params.id;
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic '+localStorage.getItem("user")
                }
            };
            fetch(path, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setState({category: data})
            });
        }else {
            this.props.history.push("/login");
        }
       

    }

    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        
        this.setState({
            category: {...this.state.category, [name]: target.value}
        });
    }    
    
    handleSubmit = (event) => {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic '+localStorage.getItem("user")
            },
            body: JSON.stringify({
                categoryName: this.state.category.categoryName,
                categoryDescription: this.state.category.categoryDescription
            })
        };
        
        const path = CATEGORIES+"/" + this.state.category.id;
        fetch( path, requestOptions)
        .then(response => 
            {
                if(response.ok) {
                    response.json().then(data => {
                        this.setState({errorMessage: ''})
                        this.props.history.push("/categories");
                    });
                }else {
                    response.text().then(message => this.setState({errorMessage: message}))
                }
            })
        .catch(error => console.log(error))
        event.preventDefault();

    };

    render() {
        return (
            <div className="login_form">
            {
                this.state.category && 
                <form onSubmit={this.handleSubmit}>
                <input 
                    type="text" 
                    name="categoryName"
                    placeholder="Change name"
                    value={this.state.category.categoryName}
                    onChange={this.handleInputChange} />

                <input 
                    type="text" 
                    name="categoryDescription" 
                    placeholder="Change description"
                    value={this.state.category.categoryDescription}
                    onChange={this.handleInputChange} />
                                    
                <input type="submit" value="Change" className="submit" />
                <input type="button" value="Cancel" className="cancel" onClick={()=>this.props.history.push("/categories")} />
                <label className="error">{this.state.errorMessage}</label>
            </form>
            }
               
            </div>
           
        )
    }
};

export default UpdateCategory;