import React, { Component } from 'react';
import '../../../style/components/auth/login.css';
import { CATEGORIES } from "../../../service/api";

class AddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {categoryName: '', categoryDescription:'', errorMessage:''};
    }

    componentDidMount() {
        const currentUser = localStorage.getItem("user");
        if(!currentUser) {
            this.props.history.push("/login");
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        
        this.setState({
            [name]: target.value
        });
    }    
    
    handleSubmit = (event) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + localStorage.getItem("user")
            },
            body: JSON.stringify({
                categoryName: this.state.categoryName,
                categoryDescription: this.state.categoryDescription
            })
        };
        
        fetch( CATEGORIES, requestOptions)
        .then(response => 
            {
                if(response.ok) {
                    response.json().then(data => {
                        this.setState({errorMessage: ''})
                        this.props.history.push("/categories");
                    });
                }else {
                    response.text().then(message => this.setState({errorMessage: message}))
                }
            })
        .catch(error => console.log(error))
        event.preventDefault();

    };

    render() {
        return (
            <div className="login_form">
                <form onSubmit={this.handleSubmit}>
                <input 
                    type="text" 
                    name="categoryName"
                    placeholder="Enter name"
                    onChange={this.handleInputChange} />

                <input 
                    type="text" 
                    name="categoryDescription" 
                    placeholder="Enter description"
                    onChange={this.handleInputChange} />
                                    
                <input type="submit" value="Add" className="submit" />
                <input type="button" value="Cancel" className="cancel" onClick={()=>this.props.history.push("/categories")} />
                <label className="error">{this.state.errorMessage}</label>
            </form> 
            </div>
           
        )
    }
};

export default AddCategory;