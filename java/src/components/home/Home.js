import React, { Component } from 'react';
import '../../style/components/home/home.css';

class Home extends Component {
  render() {
    return (
      <div className="app">
        <div className="app_title">Welcome</div>
        <div className="app_text">Your application has been successfully started</div>
      </div>
    );
  }
}

export default Home;
