import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './index.css';
import Login  from './components/auth/Login';
import Register  from './components/auth/Register';
import Subject from './components/subject/Subject';
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import UpdateSubject from './components/subject/partials/UpdateSubject';
import AddSubject from './components/subject/partials/AddSubject';
import NotFound from './components/common/NotFound';
import Home from './components/home/Home';

class App extends Component {
  render() {
    return (
        <Router>
        <Fragment>
          <Header></Header>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/subjects" component={Subject} />
            <Route exact path="/updateSubject/:id" component={UpdateSubject} />
            <Route exact path="/addSubject" component={AddSubject} />
            <Route exact path="/" component={Home} />
            <Route component={NotFound} />
          </Switch>
          <Footer></Footer>
      </Fragment>
    </Router>
    );
  }
}

export default App;
