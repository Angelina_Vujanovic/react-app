const BASE = "http://localhost:51143";

// AUTH
export const LOGIN = `${BASE}/token`;

//SUBJECTS
export const SUBJECTS = `${BASE}/school/subjects`;
export const UPDATE_SUBJECT = `${BASE}/school/subjects/updateSubject/`;