import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { SUBJECTS } from '../../service/api';
import Modal from '../common/Modal';
import '../../style/components/subject/subject.css';
import '../../style/common/table.css';


class Subject extends Component {
    constructor(props) {
        super(props);
        this.state = {subjects: [], openDialog: false, subject:null};
    }

    componentDidMount(){

        if(localStorage.getItem('token') === null) {
            this.props.history.push('/login');
        } else {
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer  '+localStorage.getItem("token")
                }
            };
            fetch(SUBJECTS, requestOptions)
            .then(response => {
                if(response.ok) {
                    response.json().then(data =>
                        this.setState({subjects: data})    
                    )
                }else {
                    response.text().then(message => alert(message))
                }
            })
            .catch(error => console.log(error))
        }    
    }
    
    openDetails = (id) => {
        let subject = this.state.subjects.find(s => s.SubjectId === id);
        this.setState({openDialog: true, subject: subject});
    }

    closeDetails = () => {
        this.setState({subject:null, openDialog:false})
    }

    updateSubject = (id) => {
        this.props.history.push("updateSubject/"+id);
    }

    deleteSubject = (id) => {
        const path = SUBJECTS + "/" + id;
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer  '+localStorage.getItem("token")
            }
        };
        fetch(path, requestOptions)
        .then(response => {
            if (response.ok) {
                response.json().then(data => 
                    this.setState({subjects: this.state.subjects.filter(subject => subject.SubjectId!==data.SubjectId)})
                );
            }else {
                response.text().then(message => alert(message))
            }
        })
        .catch(error => console.log(error))
    }

    render() {

        const heading=["Id", "Name", "SubjectLevel", "SubjectGrade", "Weekly hours", "","",""];
        const teacherHeading=["FirstName", "LastName"];
        const buttons=[
            {name: "Teachers", action: this.openDetails, class:"btn-update"},
            {name: "Update", action: this.updateSubject, class: "btn-update"}, 
            {name: "Delete", action: this.deleteSubject, class: "btn-update"}];
        
        return (
            <div className="subject_wrapper">
                <Modal show={this.state.openDialog} onClose={this.closeDetails}>
                    <table>
                        <thead>
                            <tr>
                                {
                                    teacherHeading.map((head,index) => <th key={index}>{head}</th>)
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                               this.state.subject && this.state.subject.Teachers.map(item=>
                                    <tr key={item.Id}>
                                        <td>{item.FirstName}</td>
                                        <td>{item.LastName}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </Modal>
                {
                    this.state.subjects && 
                    <table>
                        <thead>
                            <tr>
                                {heading.map((head, index) => 
                                    <th key={index}>{head}</th>    
                                )}
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.subjects.map(subject=>
                                    <tr key={subject.SubjectId}>
                                        <td>{subject.SubjectId}</td>
                                        <td>{subject.Name}</td>
                                        <td>{subject.SubjectLevel}</td>
                                        <td>{subject.SubjectGrade}</td>
                                        <td>{subject.WeeklyHoursTaught}</td>
                                        {
                                            buttons.map(btn => (
                                                <td key={btn.name}><button className={btn.class} onClick={()=>btn.action(subject.SubjectId)}>{btn.name}</button></td>
                                            ))
                                        }   
                                    </tr>  
                                     
                                )
                            }
                        </tbody>
                    </table>
                } 
                <Link to="/addSubject">Add new subject</Link>
            </div>
           
        )
    }
};

export default Subject;