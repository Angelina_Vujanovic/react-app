import React, { Component } from 'react';
import '../../../style/components/auth/login.css';
import { SUBJECTS, UPDATE_SUBJECT } from "../../../service/api";

class UpdateSubject extends Component {
    constructor(props) {
        super(props);
        this.state = {subject: null};
    }

    componentDidMount() {
        const currentUser = localStorage.getItem("token");
        if(currentUser) {
            const path = SUBJECTS+"/"+this.props.match.params.id;
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer  '+localStorage.getItem("token")
                }
            };
            fetch(path, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setState({subject: data})
            });
        }else {
            this.props.history.push("/login");
        }
       

    }

    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        
        this.setState({
            subject: {...this.state.subject, [name]: target.value}
        });
    }    
    
    handleSubmit = (event) => {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer  '+localStorage.getItem("token")
            },
            body: JSON.stringify({
                SubjectId: this.state.subject.SubjectId,
                Name: this.state.subject.Name,
                SubjectLevel: this.state.subject.SubjectLevel,
                SubjectGrade:this.state.subject.SubjectGrade,
                WeeklyHoursTaught: this.state.subject.WeeklyHoursTaught,
                Teachers: this.state.subject.Teachers
            })
        };

        const path = UPDATE_SUBJECT+ this.state.subject.SubjectId;
        fetch( path, requestOptions)
        .then(response => 
            {
                if(response.ok) {
                    response.json().then(data => {
                        this.setState({errorMessage: ''})
                        this.props.history.push("/subjects");
                    });
                }else {
                    response.text().then(message => this.setState({errorMessage: message}))
                }
            })
        .catch(error => console.log(error))
        event.preventDefault();

    };

    render() {
        const subjectLevel = ['ONEtoFOUR','FIVEtoEIGHT'];
        return (
            <div className="login_form">
            {
                this.state.subject && 
                <form onSubmit={this.handleSubmit}>
                <input 
                    type="text" 
                    name="Name"
                    placeholder="Change name"
                    value={this.state.subject.Name}
                    onChange={this.handleInputChange} />

                <select value={this.state.subject.SubjectLevel} onChange={this.handleInputChange} name="SubjectLevel">
                    {subjectLevel.map(level => 
                        <option value={level} key={level}>{level}</option>
                    )}
                </select>

                <input 
                    type="number" 
                    name="SubjectGrade" 
                    placeholder="Change subject grade"
                    value={this.state.subject.SubjectGrade}
                    onChange={this.handleInputChange} />

                <input 
                    type="number" 
                    name="WeeklyHoursTaught" 
                    placeholder="Change weekly hours"
                    value={this.state.subject.WeeklyHoursTaught}
                    onChange={this.handleInputChange} />
                                    
                <input type="submit" value="Change" className="submit" />
                <input type="button" value="Cancel" className="cancel" onClick={()=>this.props.history.push("/subjects")} />
                <label className="error">{this.state.errorMessage}</label>
            </form>
            }
               
            </div>
           
        )
    }
};

export default UpdateSubject;