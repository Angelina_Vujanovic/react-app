import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import '../../style/common/header.css';

class Header extends Component {

    logout = () => {
        localStorage.clear(); 
        this.props.history.push("/login");
    }
    
    render() {
        return (
        <div className="header">
           <Link to="/">Home</Link>
            {
                 localStorage.getItem("token") ? 
                    <div className="header_links">
                        <Link to="/subjects">Subjects</Link>
                        <div className="logout" onClick={this.logout}>Logout</div>
                    </div>
                   
                :
                    <Link to="/login">Login</Link>
            }
        </div>
        )
    }
};

export default withRouter(Header);